;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: cl-user; -*-

(defpackage "COM.SYMSIM.OSS.COLLADA-GUI"
  (:nicknames "COLLADA")
  (:use "CL" "XML")
  (:shadow
   "ARRAY"
   "COUNT"
   "FLOAT"
   "SET"
   "SYMBOL"
   "TYPE")
  )
