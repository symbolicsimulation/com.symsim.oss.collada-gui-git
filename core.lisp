(in-package :COM.SYMSIM.OSS.COLLADA-GUI)

;;
;; http://www.khronos.org/files/collada_spec_1_5.pdf
;;

;;
;; Attribute type definitions
;;

(deftype uint () 'integer)	 ; Should probably enforce unsigned...

(defmethod PRINT-TYPED-ATTRIBUTE-VALUE (Value (Type (eql 'uint)) Stream)
  (format Stream "\"~A\"" Value))

(defmethod READ-TYPED-ATTRIBUTE-VALUE ((Value string) (Type (eql 'uint)))
  (let ((val (parse-integer Value)))
    (cond ((>= val 0) val)
	  (t (error (format nil "uint ~S < 0" Value))))))

(deftype ListOfNames () 'string)
(deftype NodeType () 'string)		; enumeration: JOINT, or NODE
(deftype VersionType () 'string)
(deftype xs.anyURI () 'string)
(deftype xs.ID () 'string)
(deftype xs.NCName () 'string)
(deftype xs.NMTOKEN () 'string)
(deftype xs.short () 'fixnum)		; Hope this is OK
(deftype xs.token () 'string)		; Unique to FX image - a type typo/mistake?
(deftype xs.URIFragmentType () 'string)	; Hopefully OK



;;
;; Elements
;;

(defclass accessor (xml-serializer)
  (
   ;; Attributes
   (count :accessor com.symsim.oss.collada-gui::count :initarg :count :type uint :initform 0) ; Required
   (offset :accessor offset :initarg :offset :type uint :initform 0) ; Optional
   (source :accessor source :initarg :source :type xs.anyURI :initform "") ; Required
   (stride :accessor stride :initarg :stride :type uint :initform 1) ; Optional
   ;; Elements
   (params :accessor params :initarg :params :initform nil) ; 0 or more
   )
  )
(defmethod print-default-value-attributes-p ((self accessor)) t)

(defclass asset (xml-serializer)
  (
   ;; Attributes (NONE)
   (contributors :accessor contributors :initarg :contributors :initform nil) ; 0 or more
   (created :accessor created :initarg :created :initform "")	  ; 1
   (keywords :accessor keywords :initarg :keywords :initform nil) ; 0 or 1
   (modified :accessor modified :initarg :modified :initform nil) ; 1
   (revision :accessor revision :initarg :revision :initform nil) ; 0 or 1
   (subject :accessor subject :initarg :subject :initform nil) ; 0 or 1
   (title :accessor title :initarg :title :initform nil) ; 0 or 1
   (unit :accessor unit :initarg :unit :initform "meter") ; 0 or 1
   (up_axis :accessor up_axis :initarg :up_axis :initform nil) ; 0 or 1 of X_UP, Y_UP, Z_UP
   )
  )

(defclass authoring_tool (xml::xml-content)
  ()
  (:default-initargs :name 'authoring_tool))

(defclass collada (xml-serializer)
  (
   ;; Attributes
   ;; Switched order of first two from spec to achieve correct emission order
   (xmlns :accessor xmlns :initarg :xmlns :type xs.anyURI :initform "") ; XML Schema namespace
   (version :accessor version :initarg :version :type VersionType :initform "1.4.1")
   (base :accessor base :initarg :base :type xs.anyURI :initform "")

   ;; Child Elements
   (asset :accessor asset :initarg :asset :initform nil)
   ;; then in any order, 0 or more of each

   (library_animation_clipss
    :accessor library_animation_clipss
    :initarg :library_animation_clipss
    :type list
    :initform nil)
   (library_animationss :accessor library_animationss :initarg :library_animationss :initform nil)
   (library_camerass :accessor library_camerass :initarg :library_camerass :initform nil)
   (library_controllerss :accessor library_controllerss :initarg :library_controllerss :initform nil)
   (library_effectss :accessor library_effectss :initarg :library_effectss :initform nil) ; FX
   (library_force_fieldss		; Physics
    :accessor library_force_fieldss
    :initarg :library_force_fieldss
    :initform nil)
   (library_geometriess :accessor library_geometriess :initarg :library_geometriess :initform nil)
   (library_imagess :accessor library_imagess :initarg :library_imagess :initform nil) ; FX
   (library_lightss :accessor library_lightss :initarg :library_lightss :initform nil) ;
   (library_materialss :accessor library_materialss :initarg :library_materialss :initform nil) ; FX
   (library_nodes :accessor library_nodes :initarg :library_nodes :initform nil) ;
   (library_physics_materialss		; Physics
    :accessor library_physics_materialss
    :initarg :library_physics_materialss
    :initform nil)
   (library_physics_modelss		; Physics
    :accessor library_physics_modelss
    :initarg :library_physics_modelss
    :initform nil)
   (library_physics_sceness		; Physics
    :accessor library_physics_sceness
    :initarg :library_physics_sceness
    :initform nil)
   (library_visual_scenes :accessor library_visual_scenes :initarg :library_visual_scenes :initform nil)

   ;; Then

   (scene :accessor scene :initarg :scene :initform nil) ; 0 or 1
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )
(defmethod print-default-value-attributes-p ((self collada)) t)
(defmethod xml-tag-name-string ((self collada))	; COLLADA must be in upper-case
  (symbol-name (xml-tag-name-symbol Self)))	; Don't call
						; string-downcase as
						; in default method


(defclass color (xml-serializer)
  (
   ;; Attributes
   (sid :accessor sid :initarg :sid :type xs.NCName :initform "") ; Optional sid_type
   ;; Elements (NONE)
   )
  )

(defclass contributor (xml-serializer)
  (
   ;; Attributes (NONE)
   ;; Elements, 0 or 1 of each, in the following order
   (author :accessor author :initarg :author :initform nil)
   (authoring_tool :accessor authoring_tool :initarg :authoring_tool :initform nil)
   (comments :accessor comments :initarg :comments :initform nil)
   (copyright :accessor copyright :initarg :copyright :initform nil)
   (source_data :accessor source_data :initarg :source_data :initform nil)
   )
  )

;; date/time in ISO 8601
;; Use package zs3::parse-amazon-timestamp and zs3::iso8601-date-string for codecs
(defclass created (xml::xml-content)
  ()
  (:default-initargs :name 'created)
  )

;; Tried to make this xml-content, but AFAIK xml-content cannot have attributes
(defclass float_array (xml-serializer)
  (
   ;; Attributes
   (count :accessor com.symsim.oss.collada-gui::count :initarg :count :type uint :initform 0) ; Required
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   (digits :accessor digits :initarg :digits :type xs.short :initform 6) ; Optional
   (magnitude :accessor magnitude :initarg :magnitude :type xs.short :initform 38) ; Optional
   ;; Elements (NONE)
   ))
(defmethod print-default-value-attributes-p ((self float_array)) t)

(defclass geometry (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   ;; Elements
   (asset :accessor asset :initarg :asset :initform nil)    ; 0 or 1
   ;; One of in any order
   (convex_mesh :accessor convex_mesh :initarg :convex_mesh :initform nil)    ; 1
   (mesh :accessor mesh :initarg :mesh :initform nil)    ; 1
   (spline :accessor spline :initarg :spline :initform nil)    ; 1
   (brep :accessor brep :initarg :mesh :initform nil)    ; 1
   ;; Then
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )
   
(defclass input (xml-serializer)	; "shared" vs "unshared" in spec
  (
   ;; Attributes
   (offset :accessor offset :initarg :offset :type uint :initform 0) ; Required
   (semantic :accessor semantic :initarg :semantic :type xs.NMTOKEN :initform "") ; Required
   (source :accessor source :initarg :source :type xs.URIFragmentType :initform "") ; Required
   (set :accessor com.symsim.oss.collada-gui::set :initarg :set :type uint :initform 0) ; Optional
   ;; Elements?
   )
  )
(defmethod print-default-value-attributes-p ((self input)) t)

(defclass instance_geometry (xml-serializer)
  (
   ;; Attributes
   (sid :accessor sid :initarg :sid :type xs.NCName :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   (url :accessor url :initarg :url :type xs.anyURI :initform "") ; Required
   ;; Elements
   (bind_material :accessor bind_material :initarg :bind_material :initform nil) ; 0 or 1
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )
(defmethod print-default-value-attributes-p ((self instance_geometry)) t)

(defclass instance_node (xml-serializer)
  (
   ;; Attributes
   (sid :accessor sid :initarg :sid :type xs.NCName :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   (url :accessor url :initarg :url :type xs.anyURI :initform "")    ; Required
   ;; Elements
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )
(defmethod print-default-value-attributes-p ((self instance_node)) t)

(defclass instance_visual_scene (xml-serializer)
  (
   ;; Attributes
   (sid :accessor sid :initarg :sid :type xs.NCName :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   (url :accessor url :initarg :url :type xs.anyURI :initform "") ; Required
   ;; Elements
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )
(defmethod print-default-value-attributes-p ((self instance_visual_scene)) t)

(defclass library_animation_clips (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   ;; Elements $$$
   (assets :accessor assets :initarg :assets :initform nil) ; 0 or more
   (animation_clips :accessor animation_clips :initarg :animation_clips :initform nil) ; 1 or more
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )



(defclass library_geometries (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   ;; Elements
   (asset :accessor asset :initarg :asset :initform nil)	     ; 0 or 1
   (geometrys :accessor geometrys :initarg :geometrys :initform nil) ; 1 or more
   (extras :accessor extras :initarg :extras :initform nil)	     ; 0 or more
   )
  )

(defclass library_nodes (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   ;; Elements
   (asset :accessor asset :initarg :asset :initform nil) ; 0 or 1
   (nodes :accessor nodes :initarg :nodes :initform nil) ; 1 or more
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )
   

(defclass library_visual_scenes (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   ;; Elements
   (asset :accessor asset :initarg :asset :initform nil) ; 0 or 1
   (visual_scenes :accessor visual_scenes :initarg :visual_scenes :initform nil) ; 1 or more
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )

(defclass lines (xml-serializer)
  (
   ;; Attributes
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   (count :accessor com.symsim.oss.collada-gui::count :initarg :count :type uint :initform 0) ; Required
   (material :accessor material :initarg :material :type xs.NCName :initform "") ; Optional
   ;; Elements
   (inputs :accessor inputs :initarg :inputs :initform nil) ; 0 or more
   (p :accessor p :initarg :p :initform nil) ; Primitives 0 or 1
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )
(defmethod print-default-value-attributes-p ((self lines)) t)

(defclass matrix (xml-serializer)
  (
   ;; Attributes
   (sid :accessor sid :initarg :sid :type xs.NCName :initform "") ; Optional sid_type
   ;; Elements (NONE)
   )
  )

(defclass mesh (xml-serializer)
  (
   ;; Attributes (NONE)
   ;; Elements
   (sources :accessor sources :initarg :sources :initform nil) ; 1 or more
   (vertices :accessor vertices :initarg :vertices :initform nil) ; 1
   ;; 0 or more, any combination, in any order
   (liness :accessor liness :initarg :liness :initform nil)
   (linestripss :accessor linestripss :initarg :linestripss :initform nil)
   (polygonss :accessor polygonss :initarg :polygonss :initform nil)
   (polylists :accessor polylists :initarg :polylists :initform nil)
   (triangless :accessor triangless :initarg :triangless :initform nil)
   (trifanss :accessor trifanss :initarg :trifanss :initform nil)
   (tristripss :accessor tristripss :initarg :tristripss :initform nil)
   ;; And then
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )

;; date/time in ISO 8601
;; again use zs3
(defclass modified (xml::xml-content)
  ()
  (:default-initargs :name 'modified))

(defclass node (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   (sid :accessor sid :initarg :sid :type xs.NCName :initform "") ; Optional
   (type :accessor com.symsim.oss.collada-gui::type :initarg type :type NodeType :initform "NODE") ; Optional
   (layer :accessor layer :initarg :layer :type ListOfNames :initform "") ; Optional
   ;; Elements
   (asset :accessor asset :initarg :asset :initform nil) ; 0 or 1
   ;; Next any combination of 0 or more transformation elements
   (lookats :accessor lookats :initarg :lookats :initform nil)
   (matrixs :accessor matrixs :initarg :matrixs :initform nil)
   (rotates :accessor rotates :initarg :rotates :initform nil)
   (scales :accessor scales :initarg :scales :initform nil)
   (skews :accessor skews :initarg :skews :initform nil)
   (translates :accessor translates :initarg :translates :initform nil)
   ;; Then 0 or more of each in this order
   (instance_cameras :accessor instance_cameras :initarg :instance_camerasgeometrys :initform nil)
   (instance_controllers :accessor instance_controllers :initarg :instance_controllers :initform nil)
   (instance_geometrys :accessor instance_geometrys :initarg :instance_geometrys :initform nil)
   (instance_lights :accessor instance_lights :initarg :instance_lights :initform nil)
   (instance_nodes :accessor instance_nodes :initarg :instance_nodes :initform nil)
   (nodes :accessor nodes :initarg nodes :initform nil) ; 0 or more
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )

;; Primitives - indices only in mesh
(defclass p (xml::xml-content)
  ()
  (:default-initargs :name 'p))

(defclass param (xml-serializer)
  (
   ;; Attributes
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   (sid :accessor sid :initarg :sid :type xs.NCName :initform "") ; Optional
   (type :accessor com.symsim.oss.collada-gui::type :initarg type :type xs.NMTOKEN :initform "") ; Required
   (semantic :accessor semantic :initarg :semantic :type xs.NMTOKEN :initform "") ; Optional
   ;; Elements?
   )
  )
(defmethod print-default-value-attributes-p ((self param)) t)

(defclass scene (xml-serializer)
  (
   ;; Attributes (NONE)
   ;; Elements
   (instance_physics_scenes		; 0 or more in Physics
    :accessor instance_physics_scenes
    :initarg :instance_physics_scenes
    :initform nil)
   (instance_visual_scene		; 0 or 1
    :accessor instance_visual_scene
    :initarg :instance_visual_scene
    :initform nil)
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )

(defclass source (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "")
   (name :accessor name :initarg :name :type xs.NCName :initform "")
   ;; Elements
   (asset :accessor asset :initarg :asset :initform nil) ; 0 or 1
   ;; One of 
   (IDREF_array :accessor IDREF_array :initarg :IDREF_array :initform nil)
   (Name_array :accessor Name_array :initarg :Name_array :initform nil)
   (bool_array :accessor bool_array :initarg :bool_array :initform nil)
   (float_array :accessor float_array :initarg :float_array :initform nil)
   (int_array :accessor int_array :initarg :int_array :initform nil)
   ;; Then
   (technique_common :accessor technique_common :initarg :technique_common :initform nil) ; 0 or 1
   (techniques :accessor techniques :initarg :techniques :initform nil) ; 0 or more
   )
  )

(defclass technique_common (xml-serializer)
  (
   ;; Attributes are "unique" (??)
   ;; Elements based upon samples, not upon the spec (which is deliciously opaque).
   (instance_material :accessor instance_material :initarg :instance_material :initform nil)
   (accessor :accessor accessor :initarg :accessor-initarg :initform nil) ; How should initarg work?
   )
  )

(defclass triangles (xml-serializer)
  (
   ;; Attributes
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   (count :accessor com.symsim.oss.collada-gui::count :initarg :count :type uint :initform 0) ; Required
   (material :accessor material :initarg :material :type xs.NCName :initform "") ; Optional
   ;; Elements
   (inputs :accessor inputs :initarg :inputs :initform nil) ; 1 or more
   (p :accessor p :initarg :p :initform nil) ; Primitives 0 or 1
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )
(defmethod print-default-value-attributes-p ((self triangles)) t)

(defclass unit (xml-serializer)
  (
   (name :accessor name :initarg :name :type xs.NMTOKEN :initform "meter")
   (meter :accessor meter :initarg :meter :type single-float :initform 1.0)
   )
  )

;; X_UP, Y_UP, or Z_UP
(defclass up_axis (xml::xml-content)
  ()
;;  (:default-initargs :name 'up_axis :content "Y_UP")
  (:default-initargs :name 'up_axis)
  )

(defclass vertices (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Required
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   ;; Elements
   (inputs :accessor inputs :initarg :inputs :initform nil) ; 1 or more
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )
(defmethod print-default-value-attributes-p ((self vertices)) t)

(defclass visual_scene (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   ;; Elements
   (asset :accessor asset :initarg :asset :initform nil)    ; 0 or 1
   (nodes :accessor nodes :initarg :nodes :initform nil)   ; 1 or more
   (evaluate_scenes :accessor evaluate_scenes :initarg :evaluate_scenes :initform nil) ; 0 or more
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )
