;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

;;
;;
;; sbcl --core xmlisp.core --load com.symsim.oss.collada-gui.asd --eval "(asdf:oos 'asdf:load-op :com.symsim.oss.collada-gui)"
;;
;; Requires: XMLisp (which is not, as of this writing, available via quicklisp.
;;

(defpackage #:com.symsim.oss.collada-gui.asd
;  (:use :cl :asdf :xml)
  (:use :cl :asdf)
  )

(in-package :com.symsim.oss.collada-gui.asd)

(defsystem com.symsim.oss.collada-gui
    :name "com.symsim.oss.collada-gui"
    :version "0.0.1"
    :maintainer "jm@symbolic-simulation.com"
    :author "jm@symbolic-simulation.com"
    :licence "Golden Rule License"
    :description "An attempt at a COLLADA GUI."
;    :depends-on (:weblocks :cl-graph)
;    :depends-on (:xmlisp)
    :components (
		 (:file "package")
		 (:file "core" :depends-on ("package"))
		 (:file "brep" :depends-on ("package" "core"))
		 (:file "fx" :depends-on ("package" "core"))
		 (:file "physics" :depends-on ("package" "core"))
		 (:file "test" :depends-on ("package" "core" "brep" "fx" "physics"))
		 )
    )

