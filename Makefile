run:
	rm -fv samples/tmp2.dae
#	rm -fv samples/white-cube.dae
	sbcl --core xmlisp.core \
             --load com.symsim.oss.collada-gui.asd \
             --eval "(asdf:oos 'asdf:load-op :com.symsim.oss.collada-gui)" \
             --eval "(sb-ext:quit)"
	xmllint --pretty 2 samples/tmp2.dae | less
#	xmllint --pretty 2 samples/white-cube.dae

core:
	sbcl --load build-core.lisp


tags:
	sbcl --eval "(ql:quickload :com.symsim.oss.collada-gui)" --eval "(make-tags :com.symsim.oss.collada-gui)"
