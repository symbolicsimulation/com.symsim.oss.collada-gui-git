(in-package :XML)

(defpackage "COLLADA"
  (:use "XML")
  (:shadow
   "COUNT"
   "SET"
   "SYMBOL"
   "TYPE")
  )

;;
;; http://www.khronos.org/files/collada_spec_1_5.pdf
;;

(defclass accessor (xml-serializer)
  (
   ;; Attributes
   (count :accessor com.symsim.oss.collada-gui::count :initarg :count :initform 0) ; Required uint_type
   (offset :accessor offset :initarg :offset :initform 0)	; Optional uint_type
   (source :accessor source :initarg :source :initform nil)	; Required xs:anyURI
   (stride :accessor stride :initarg :stride :initform 1)	; Optional uint_type
   ;; Elements
   (params :accessor params :initarg :params :initform nil) ; 0 or more
   )
  )

(defclass asset (xml-serializer)
  (
   (contributor :accessor contributor :initarg :contributor :initform nil)
   (coverage :accessor coverage :initarg :coverage :initform nil) ; ??? geographic location?
   (created :accessor created :initarg :created :initform "")
   (keywords :accessor keywords :initarg :keywords :initform nil)
   (modified :accessor modified :initarg :modified :initform nil)
   (unit :accessor unit :initarg :unit :initform nil)
   (up_axis :accessor up_axis :initarg :up_axis :initform nil)
   )
  )

(defclass authoring_tool (xml-serializer) ())

(defclass collada (xml-serializer)
  (
   ;; Attributes
   (version :accessor version :initarg :version :initform nil) ; Enumeration. Number?
   (xmlns :accessor xmlns :initarg :xmlns :initform nil) ; XML Schema namespace
   (base :accessor base :initarg :base :initform nil) ; XML Base spec

   ;; Child Elements
   (asset :accessor asset :initform nil)
   ;; then in any order
   (library_visual_scenes :accessor library_visual_scenes :initarg :library_visual_scenes :initform nil)
   (library_geometries :accessor library_geometries :initarg :library_geometries :initform nil)
;   (scene :accessor scene :initform nil)
;   (extra :accessor extra :initform nil)
  )
  )

(defclass contributor (xml-serializer)
  (
   (author :accessor author :initarg :author :initform "")
   (author_email :accessor author_email :initarg :author_email :initform "")
   (author_website :accessor author_website :initarg :author_website :initform nil)
   (authoring_tool :accessor authoring_tool :initarg :authoring_tool :initform "")
   (comments :accessor comments :initarg :comments :initform "")
   (copyright :accessor copyright :initarg :copyright :initform "")
   (source_data :accessor source_data :initarg :source_data :initform nil)
   )
  )

(defclass created (xml-serializer) ())

(defclass float_array (xml-serializer)
  (
   ;; Attributes
   (count :accessor com.symsim.oss.collada-gui::count :initarg :count :initform 0) ; Required uint_type
   (id :accessor id :initarg :id :initform nil)		 ; Optional xs:ID
   (name :accessor name :initarg :name :initform nil)	 ; Optional xs:token
   (digits :accessor digits :initarg :digits :initform 0) ; Optional xs:unsignedByte
   (magnitude :accessor magnitude :initarg :magnitude :initform 0) ; Optional xs:short
   )
  )

(defclass geometry (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :initform nil)		 ; xs:ID
   (name :accessor name :initarg :name :initform nil)	 ; xs:token
   ;; Elements
   (asset :accessor asset :initarg :asset :initform nil)    ; 0 or 1
   ;; One of in any order
   (convex_mesh :accessor convex_mesh :initarg :convex_mesh :initform nil)    ; 1
   (mesh :accessor mesh :initarg :mesh :initform nil)    ; 1
   (spline :accessor spline :initarg :spline :initform nil)    ; 1
   (brep :accessor brep :initarg :mesh :initform nil)    ; 1
   ;; Then
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )
   
(defclass input (xml-serializer)	; "shared" vs "unshared" in spec
  (
   ;; Attributes
   (offset :accessor offset :initarg :offset :initform 0)	; Required uint_type
   (semantic :accessor semantic :initarg :semantic :initform nil) ; Required xs:NMTOKEN
   (source :accessor source :initarg :source :initform nil)	; Required urifragment_type
   (set :accessor com.symsim.oss.collada-gui::set :initarg :set :initform nil)	; Optional uint_type
   ;; Elements?
   )
  )

(defclass instance_geometry (xml-serializer)
  (
   ;; Attributes
   (sid :accessor sid :initarg :sid :initform nil)
   (name :accessor name :initarg :name :initform nil)
   (url :accessor url :initarg :url :initform nil)
   ;; Elements
   (bind_material :accessor bind_material :initarg :bind_material :initform nil)
   )
  )

(defclass instance_node (xml-serializer)
  (
   ;; Attributes
   (sid :accessor sid :initarg :sid :initform nil) ; sid_type
   (name :accessor name :initarg :name :initform nil) ; xs:token
   (url :accessor url :initarg :url :initform nil)    ; xs:anyURI
   (proxy :accessor proxy :initarg :proxy :initform nil)    ; xs:anyURI
   ;; Elements
   (extras :accessor extras :initarg :extras :initform nil)
   )
  )

(defclass library_geometries (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :initform nil) ; xs:ID
   (name :accessor name :initarg :name :initform nil) ; xs:token
   ;; Elements
   (asset :accessor asset :initarg :asset :initform nil)	     ; 0 or 1
   (geometrys :accessor geometrys :initarg :geometrys :initform nil) ; 1 or more
   (extras :accessor extras :initarg :extras :initform nil)	     ; 0 or more
   )
  )

(defclass library_visual_scenes (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :initform nil)
   (name :accessor name :initarg :name :initform nil)
   ;; Elements
   (visual_scenes :accessor visual_scenes :initarg :visual_scenes :initform nil)
   )
  )

(defclass lines (xml-serializer)
  (
   ;; Attributes
   (name :accessor name :initarg :name :initform nil) ; Optional xs:token
   (count :accessor com.symsim.oss.collada-gui::count :initarg :count :initform nil) ; Required uint_type
   (material :accessor material :initarg :material :initform nil) ; Optional.
   ;; Elements
   (inputs :accessor inputs :initarg :inputs :initform nil) ; 0 or more
   (p :accessor p :initarg :p :initform nil) ; Primitives 0 or 1
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )

(defclass mesh (xml-serializer)
  (
   ;; Attributes - none
   ;; Elements
   (sources :accessor sources :initarg :sources :initform nil) ; 1 or more
   (vertices :accessor vertices :initarg :vertices :initform nil) ; 1
   ;; Any combination in any order
   ;; Should I be using plurals (in some cases, of plurals)??
   (lines :accessor lines :initarg :lines :initform nil) ; 0 or more
   (linestrips :accessor linestrips :initarg :linestrips :initform nil) ; 0 or more
   (polygons :accessor polygons :initarg :polygons :initform nil) ; 0 or more
   (polylist :accessor polylist :initarg :polylist :initform nil) ; 0 or more
   (triangles :accessor triangles :initarg :triangles :initform nil) ; 0 or more
   (trifans :accessor trifans :initarg :trifans :initform nil) ; 0 or more
   (tristrips :accessor tristrips :initarg :tristrips :initform nil) ; 0 or more
   )
  )

(defclass matrix (xml-serializer)
  (
   ;; Attributes
   (sid :accessor sid :initarg :sid :initform nil) ; sid_type
   )
  )

(defclass modified (xml-serializer) ())

(defclass node (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :initform nil)
   (name :accessor name :initarg :name :initform nil)
   (sid :accessor sid :initarg :sid :initform nil)
   ;; type!
   (layer :accessor layer :initarg :layer :initform nil)
   ;; Elements (incomplete)
   (asset :accessor asset :initarg :asset :initform nil) ; 0 or 1
   (instance_geometrys :accessor instance_geometrys :initarg :instance_geometrys :initform nil)
   (instance_nodes :accessor instance_nodes :initarg :instance_nodes :initform nil)
   (matrixs :accessor matrixs :initarg matrixs :initform nil) ; 0 or more
   (nodes :accessor nodes :initarg nodes :initform nil) ; 0 or more
   )
  )

(defclass p (xml-serializer) ())	; Primitives - indices only in mesh

(defclass param (xml-serializer)
  (
   ;; Attributes
   (name :accessor name :initarg :name :initform nil) ; Optional xs:token
   (sid :accessor sid :initarg :sid :initform "") ; Optional sid_type
   (type :accessor com.symsim.oss.collada-gui::type :initarg type :initform "") ; Required xs:NMTOKEN
   (semantic :accessor semantic :initarg :semantic :initform nil) ; Optional xs:NMTOKEN
   ;; Elements?
   )
  )

(defclass source (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :initform nil)
   (name :accessor name :initarg :name :initform nil)
   ;; Elements
   ;; One of 
   (float_array :accessor float_array :initarg :float_array :initform nil)
   ;; Then
   (technique_common :accessor technique_common :initarg :technique_common :initform nil) ; 0 or 1
   )
  )

(defclass technique_common (xml-serializer)
  (
   ;; Elements based upon samples, not upon the spec (which is deliciously opaque).
   (instance_material :accessor instance_material :initarg :instance_material :initform nil)
   (accessor :accessor accessor :initarg :accessor-initarg :initform nil) ; How should initarg work?
   )
  )

(defclass triangles (xml-serializer)
  (
   ;; Attributes
   (name :accessor name :initarg :name :initform nil) ; Optional xs:token
   (count :accessor com.symsim.oss.collada-gui::count :initarg :count :initform nil) ; Required uint_type
   (material :accessor material :initarg :material :initform nil) ; Optional.
   ;; Elements
   (inputs :accessor inputs :initarg :inputs :initform nil) ; 1 or more
   (p :accessor p :initarg :p :initform nil) ; Primitives 0 or 1
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )

(defclass unit (xml-serializer)
  ((meter :accessor meter :initarg :meter :initform 1.0)
   (name :accessor name :initarg :name :initform "")))

(defclass up_axis (xml-serializer) ())

(defclass vertices (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :initform nil) ; Required xs:ID
   (name :accessor name :initarg :name :initform nil) ; Optional xs:token
   ;; Elements
   (inputs :accessor inputs :initarg :inputs :initform nil) ; 1 or more
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )

(defclass visual_scene (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :initform nil)
   (name :accessor name :initarg :name :initform nil)
   ;; Elements
   (nodes :accessor nodes :initarg :nodes :initform nil))
  )

;;
;; COLLADA FX
;;

(defclass bind_material (xml-serializer)
  ;; Elements only
  (
   (technique_common :accessor technique_common :initarg :technique_common :initform nil)
  )
  )

(defclass bind_vertex_input (xml-serializer)
  (
   ;; Attributes
   (semantic :accessor semantic :initarg :semantic :initform nil) ; xs:NCName
   (input_semantic :accessor input_semantic :initarg :input_semantic :initform nil) ; xs:NCName
   (input_set :accessor input_set :initarg :input_set :initform nil) ; uint_type
   )
  )

(defclass instance_material (xml-serializer)
  (
   ;; Attributes
   (sid :accessor sid :initarg :sid :initform "") ; scoped identifier
   (name :accessor name :initarg :name :initform "")
   (target :accessor target :initarg :target :initform nil) ; xs:anyURI URI of <material>
   (symbol :accessor com.symsim.oss.collada-gui::symbol :initarg :symbol :initform nil) ; xs:NCName
   ;; Elements (skipping bind and extra)
   (bind_vertex_input :accessor bind_vertex_input :initarg :bind_vertex_input :initform nil)
   )
  )

(defclass library_materials (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :initform nil) ; Optional xs:ID
   (name :accessor name :initarg :name :initform nil) ; Optional xs:token
   ;; Elements
   (asset :accessor asset :initarg :asset :initform nil)    ; 0 or 1
   (materials :accessor materials :initarg :materials :initform nil) ; 1 or more
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )

(defclass material (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :initform nil) ; Optional xs:ID
   (name :accessor name :initarg :name :initform nil) ; Optional xs:token
   ;; Elements
   (asset :accessor asset :initarg :asset :initform nil)    ; 0 or 1
   (instance_effect :accessor instance_effect :initarg :instance_effect :initform nil) ; 1
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )
;;
;;
;;

(defparameter m1 (load-object "/home/jm/Downloads/Collada/M1A1.dae"))

(clouseau:inspector m1)

