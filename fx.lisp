(in-package :COM.SYMSIM.OSS.COLLADA-GUI)

;;
;; COLLADA FX
;;

(defclass bind_material (xml-serializer)
  (
   ;; Attributes (NONE)
   ;; Elements in the following order - params and technique in core
   (params :accessor params :initarg :params :initform nil) ; 0 or more
   (technique_common :accessor technique_common :initarg :technique_common :initform nil) ; 1
   (techniques :accessor techniques :initarg :techniques :initform nil) ; 0 or more
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )

(defclass bind_vertex_input (xml-serializer)
  (
   ;; Attributes
   (semantic :accessor semantic :initarg :semantic :type xs.NCName :initform "") ; Required
   (input_semantic			; Required
    :accessor input_semantic
    :initarg :input_semantic
    :type xs.NCName
    :initform "")
   (input_set :accessor input_set :initarg :input_set :type uint :initform 0) ; Optional
   )
  )
(defmethod print-default-value-attributes-p ((self bind_vertex_input)) t)

;;
;; common_color_or_texture_type
;;

(defclass common_color_or_texture_type (xml-serializer)
  (
   ;; Elements exactly one of
   (color :accessor color :initarg :color :initform nil)
   (param :accessor param :initarg :param :initform nil)
   (texture :accessor texture :initarg :texture :initform nil)
   )
  )

(defclass diffuse (common_color_or_texture_type) ())

(defclass transparent (common_color_or_texture_type)
  (
   ;; Attributes
   (opaque :accessor opaque :initarg :opaque :initform nil) ; Enumeration A_ONE RGB_ZERO Z_ERO RGB_ONE
   )
  )

;;
;; common_float_or_param_type
;;

(defclass com.symsim.oss.collada-gui::float (xml-serializer) ())

(defclass common_float_or_param_type (xml-serializer)
  (
   ;; Elements only
   (com.symsim.oss.collada-gui::float :accessor com.symsim.oss.collada-gui::float :initarg :float :initform nil)
   (param :accessor param :initarg :param :initform nil)
   )
  )

(defclass transparency (common_float_or_param_type) ())

;;
;;
;;

(defclass constant (xml-serializer)
  (
   ;; Attributes (NONE)
   ;; Elements, 0 or 1 of each in the following order
   (emission :accessor emission :initarg :emission :initform nil)
   (reflective :accessor reflective :initarg :reflective :initform nil)
   (reflectivity :accessor reflectivity :initarg :reflectivity :initform nil)
   (transparent :accessor transparent :initarg :transparent :initform nil)
   (transparency :accessor transparency :initarg :transparency :initform nil)
   (index_of_refraction :accessor index_of_refraction. :initarg :index_of_refraction. :initform nil)
   )
  )

;;
;; fx_sampler_common
;;

(defclass fx_sampler_common (xml-serializer) ()) ; INCOMPLETE!

(defclass sampler2d (fx_sampler_common)	; INCOMPLETE!
  (
   ;; Elements
   ;; Hah - this isn't in spec 1.5 but in 1.4.1?
   (source :accessor source :initarg :source :initform nil)
   )
  )
   

;;
;;

(defclass effect (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Required
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   ;; Elements
   (asset :accessor asset :initarg :asset :initform nil)    ; 0 or 1
   (annotates :accessor annotates :initarg :annotates :initform nil) ; 0 or more
   (images :accessor images :initarg :images :initform nil) ; 0 or more
   (newparams :accessor newparams :initarg :newparams :initform nil) ; 0 or more
   ;; At least one profile
   (profile_CG :accessor profile_CG :initarg :profile_CG :initform nil)
   (profile_GLES :accessor profile_GLES :initarg :profile_GLES :initform nil)
   (profile_GLSL :accessor profile_GLSL :initarg :profile_GLSL :initform nil)
   (profile_COMMON :accessor profile_COMMON :initarg :profile_COMMON :initform nil)
   ;; And then
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )
(defmethod print-default-value-attributes-p ((self effect)) t)

(defclass image (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   (format :accessor name :initarg :name :type xs.token :initform "") ; Optional e.g. "JPG"
   (height :accessor height :initarg :height :initform 0) ; Optional
   (width :accessor width :initarg :width :initform 0) ; Optional
   (depth :accessor depth :initarg :depth :initform 1) ; Optional
   ;; Elements - no more than one of init_from or create_*
   (asset :accessor asset :initarg :asset :initform nil)    ; 0 or 1
   ;; The one of
   (data :accessor data :initarg :data :initform nil)
   (init_from :accessor init_from :initarg :init_from :initform nil)
   ;; Then
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )

(defclass init_from (xml::xml-content)
  ()
  (:default-initargs :name 'init_from))

(defclass instance_effect (xml-serializer)
  (
   ;; Attributes
   (sid :accessor sid :initarg :sid :initform "") ; Optional sid_type
   (name :accessor name :initarg :name :initform "") ; Optional xs:token
   (url :accessor url :initarg :url :initform nil) ; xs:anyURI URI of <effect>
   ;; Elements
   (technique_hints :accessor technique_hints :initarg :technique_hints :initform nil) ; 0 or more
   (setparams :accessor setparams :initarg :setparams :initform nil) ; 0 or more
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )

(defclass instance_material (xml-serializer)
  (;; Attributes
   (sid :accessor sid :initarg :sid :type xs.NCName :initform "") ; Required
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   (target :accessor target :initarg :target :type xs.anyURI :initform "") ; Required
   (symbol :accessor com.symsim.oss.collada-gui::symbol :initarg :symbol :type xs.NCName :initform "") ; Required
   ;; Elements, 0 or more of each, in the following order
   (binds :accessor binds :initarg :binds :initform nil)
   (bind_vertex_inputs :accessor bind_vertex_inputs :initarg :bind_vertex_inputs :initform nil)
   (extras :accessor extras :initarg :extras :initform nil)))
(defmethod print-default-value-attributes-p ((self instance_material)) t)

(defclass lambert (xml-serializer)
  (;; Attributes (NONE)
   ;; Elements (0 or 1 of each)
   (emission :accessor emission :initarg :emission :initform nil)
   (ambient :accessor ambient :initarg :ambient :initform nil) ; FX
   (diffuse :accessor diffuse :initarg :diffuse :initform nil)
   (reflective :accessor reflective :initarg :reflective :initform nil)
   (reflectivity :accessor reflectivity :initarg :reflectivity :initform nil)
   (transparent :accessor transparent :initarg :transparent :initform nil)
   (transparency :accessor transparency :initarg :transparency :initform nil)
   (index_of_refraction :accessor index_of_refraction. :initarg :index_of_refraction. :initform nil)))

(defclass library_effects (xml-serializer)
  (;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   ;; Elements in the following order
   (asset :accessor asset :initarg :asset :initform nil)    ; 0 or 1
   (effects :accessor effects :initarg :effects :initform nil) ; 1 or more
   (extras :accessor extras :initarg :extras :initform nil))) ; 0 or more

(defclass library_images (xml-serializer)
  (;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   ;; Elements in the following order
   (asset :accessor asset :initarg :asset :initform nil)    ; 0 or 1
   (images :accessor images :initarg :images :initform nil) ; 1 or more
   (extras :accessor extras :initarg :extras :initform nil))) ; 0 or more

(defclass library_materials (xml-serializer)
  (;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   ;; Elements in the following order
   (asset :accessor asset :initarg :asset :initform nil)    ; 0 or 1
   (materials :accessor materials :initarg :materials :initform nil) ; 1 or more
   (extras :accessor extras :initarg :extras :initform nil))) ; 0 or more

(defclass material (xml-serializer)
  (;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Optional
   (name :accessor name :initarg :name :type xs.NCName :initform "") ; Optional
   ;; Elements in the following order
   (asset :accessor asset :initarg :asset :initform nil)    ; 0 or 1
   (instance_effect :accessor instance_effect :initarg :instance_effect :initform nil) ; 1
   (extras :accessor extras :initarg :extras :initform nil))) ; 0 or more

(defclass newparam (xml-serializer)
  (;; Attributes
   ;; says it is xs:NCName in GLES and COMMON, but xs:token elsewhere!
   (sid :accessor sid :initarg :sid :type xs.NCName :initform "") ; Required
   ;; Elements in the following order

   ;; The first two do NOT exist in profile_COMMON
   (annotates :accessor annotates :initarg :annotates :initform nil)    ; 0 or more
   (semantic :accessor semantic :initarg :semantic :initform nil) ; 0 or 1
   (modifier :accessor modifier :initarg :modifier :initform nil) ; 0 or 1
   ;; in profile_CG scope, Then exactly one of ...
   (array :accessor com.symsim.oss.collada-gui::array :initarg :array :initform nil)
   (cg_value_type :accessor cg_value_type :initarg :cg_value_type :initform nil) ; Ambiguous in spec
   (usertype :accessor usertype :initarg :usertype :initform nil)

   ;; in profile_GLSL, then 
   ;; array
   (glsl_value_type :accessor glsl_value_type :initarg :glsl_value_type :initform nil) ; Ambiguous

   ;; in profile_GLES, then
   (core_value_type :accessor core_value_type :initarg :core_value_type :initform nil) ; Ambiguous
   (gles_value_type :accessor gles_value_type :initarg :gles_value_type :initform nil) ; Ambiguous

   ;; in profile_COMMON
   (float :accessor com.symsim.oss.collada-gui::float :initarg :float :initform nil)
   (float2 :accessor float2 :initarg :float2 :initform nil)
   (float3 :accessor float3 :initarg :float3 :initform nil)
   (float4 :accessor float4 :initarg :float4 :initform nil)
   (surface :accessor surface :initarg :surface :initform nil) ;
   (sampler2d :accessor sampler2d :initarg :sampler2d :initform nil)))
(defmethod print-default-value-attributes-p ((self newparam)) t)

(defclass profile_common (xml-serializer)
  (
   ;; Attributes
   (id :accessor id :initarg :id :initform nil) ; Optional xs:ID
   ;; Elements
   (asset :accessor asset :initarg :asset :initform nil)    ; 0 or 1
   (newparams :accessor newparams :initarg :newparams :initform nil) ; 0 or more
   (technique :accessor technique :initarg :technique :initform nil) ; 1
   (extras :accessor extras :initarg :extras :initform nil) ; 0 or more
   )
  )

(defclass texture (xml-serializer)
  (
   ;; Attributes
   (texture :accessor texture :initarg :texture :type xs.NCName :initform "") ; Required
   (texcoord :accessor texcoord :initarg :texcoord :type xs.NCName :initform "") ; Required
   ;; Elements (I find this ambiguous)
   (extra :accessor extra :initarg :extra :initform nil) ; 0 or 1?
   )
  )
(defmethod print-default-value-attributes-p ((self texture)) t)

(defclass technique (xml-serializer)
  (;; Attributes
   (id :accessor id :initarg :id :type xs.ID :initform "") ; Required
   (sid :accessor sid :initarg :sid :type xs.NCName :initform "") ; Required
   ;; Elements
   (asset :accessor asset :initarg :asset :initform nil)    ; 0 or 1
   (annotates :accessor annotates :initarg :annotates :initform nil) ; 0 or more
   (includes :accessor includes :initarg :includes :initform nil) ; 0 or more
   (codes :accessor codes :initarg :codes :initform nil) ; 0 or more
   (newparams :accessor newparams :initarg :newparams :initform nil) ; 0 or more
   (setparams :accessor setparams :initarg :setparams :initform nil) ; 0 or more
   (images :accessor images :initarg :images :initform nil) ; 0 or more
   (blinn :accessor blinn :initarg :blinn :initform nil) ; 1
   (constant :accessor constant :initarg :contributor :initform nil) ; 1 (FX)
   (lambert :accessor lambert :initarg :lambert :initform nil) ; 1
   (phong :accessor phong :initarg :phong :initform nil) ; 1
   (passs :accessor passs :initarg :passs :initform nil) ; 1 or more
   (extras :accessor extras :initarg :extras :initform nil))) ; 0 or more
(defmethod print-default-value-attributes-p ((self technique)) t)
