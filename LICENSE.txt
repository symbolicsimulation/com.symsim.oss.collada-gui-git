Golden Rule License (Version 2)

"Do unto others as you would have them do unto you."

The intent of this license is two-fold:

(1) We hope this software is useful to you.  If you, in turn, increase
its usefulness, then we would like you to return the favor by
providing that increased usefulness back to us in source code form.
To this end, UNPAID users may use this software under the AGPLv3.
Examples include: students, unpaid instructors, hobbyists, unpaid
charitable individual workers, etc.

(2) All of us need to eat.  If this software helps you eat (i.e., get
paid), then we would like you to return the favor.  To this end, PAID
users may use this software under a commercial license.  Examples
include: resellers, providers of software-as-a-service, paid
instructors, paid non-profit workers etc.

This software Copyright Symbolic Simulation, LLC, 2012.

THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL SYMBOLIC SIMULATION, LLC BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
