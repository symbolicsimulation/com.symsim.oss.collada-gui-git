#|
(ql:quickload :closer-mop)
(ql:quickload :xml-mop)
(ql:quickload :parse-number)
(ql:quickload :net-telent-date)
|#

(ql:quickload :zs3)
(ql:quickload :clouseau)
(ql:quickload :mcclim)
(ql:quickload :clim-examples)
(ql:quickload :clim-listener)

(defpackage :XML
  (:export "XML-SERIALIZER" "PARSE-FILE-NAME")
  (:use "COMMON-LISP")
  )

(load "/home/dev/3rdparty/Lisp/xmlisp-read-only/XMLisp/sources/XMLisp/XMLisp.lisp")

(save-lisp-and-die "xmlisp.core")
