(in-package :COM.SYMSIM.OSS.COLLADA-GUI)

;;
;; COLLADA B-Rep (Boundary Representation)
;;

(defclass surface (xml-serializer)
  (
   ;; Attributes
   (sid :accessor sid :initarg :sid :initform "") ; Optional sid_type
   (name :accessor name :initarg :name :initform "") ; Optional xs:token

   ;; Hah - this isn't in spec 1.5 but in 1.4.1?
   (type :accessor com.symsim.oss.collada-gui::type :initarg type :initform "") ; Required xs:NMTOKEN

   ;; Elements - exactly one of the following

   ;; Hah - this isn't in spec 1.5 but in 1.4.1?
   (init_from :accessor init_from :initarg :init_from :initform nil) ; FX
   )
  )
