(in-package :COM.SYMSIM.OSS.COLLADA-GUI)

;;
;;
;;

(defparameter m1 nil)

;(inspect m1)
;(clouseau:inspector m1)


(defun run ()
  (setf m1 (load-object "samples/M1A1.dae"))
  (save-object m1 "samples/tmp2.dae" :verbose nil))
